# a-nh conda recipe

Home: "https://gitlab.esss.lu.se/nice/conda-bot-testbed/testbed-modules/a-nh"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS a-nh module
